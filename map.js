

function map(elements = [], cb = undefined) {
  if ((!cb) || (!(Array.isArray(elements))) || (!elements)) {
      return [];
  }
  else {
      let array = [];
      for(index=0; index<elements.length; index++) {
          let item = elements[index];
          let mapping = cb(item, index, elements);
          array.push(mapping)
      }
      return array;
  }
}

module.exports = map;


