function find(elements, cb) {
  if (!Array.isArray(elements) || !elements || !cb) {
    return undefined;
  }
  for (let index = 0; index < elements.length; index++) {
    let item = elements[index];
    if (cb(item, index, elements)) {
      return item;
    }
  }
  return undefined;
}

module.exports = find;


