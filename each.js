function reduce(elements, cb) {
  if (!Array.isArray(elements) || !cb || !elements) {
    return [];
  }
  for (let index = 0; index < elements.length; index++) {
    let item = elements[index];
    elements[index] = cb(item, index, elements);
  }
  return elements;
}

module.exports = reduce;

