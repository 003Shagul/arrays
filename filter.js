function filter(elements, cb) {
  if ((!Array.isArray(elements)) || (!elements) || (!cb)) {
    return [];
  }
  let array = [];
  for (let index = 0; index < elements.length; index++) {
    let item = elements[index];
    let filtering = cb(item, index, elements);
    if (filtering===true) {
      array.push(item);
    }
  }
  return array;
}

module.exports = filter;