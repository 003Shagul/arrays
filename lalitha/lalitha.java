package er.automation.pages;

import er.automation.engine.helpers.AutomationUtils;
import er.automation.engine.setup.Step;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class MrCreationpage extends Step {
    static Logger log = LogManager.getLogger(ProcurementPage.class);

    
    public static final By newButton = By.xpath("//span[normalize-space()='New']");
    public static final By nameId = By.xpath("(//div[@class='ellipsis title-text'])");
    public static final By ID = By.xpath("//input[@data-fieldname='name']");
    public static final By pasteMRID = By.xpath("(//input[@class='input-with-feedback form-control input-sm'])[4]");
    public static final String nameLocator = "//a[@title='%s']";
    public static final By mrStatus = By.xpath("(//span[@class='hidden-xs'])");
    public static final String mrLocator = "//a[contains(@href, #Form/Material Request/%s)]";
    public static final By createForWarehouse = By.xpath("//button[text()='Create for Warehouse']");
    public static final By warehouseForAutomationMR=By.xpath("(//label[text()='Warehouse'])[2]/following::input");
    public static final By create=By.xpath("//button[text()='Create']");
    public static final By save=By.xpath("//span[text()='Save']");
    public static final By close=By.xpath("//span[text()='Close']");
   
    public String procurementProjectionLogID;
    public String materialRequestID;

    public void clickNew(){
        getUiActions().click(newButton);
    }

    public void enterWarehouse(String warehouse){
        getUiActions().typeByAttributeNameAndHitTab(warehouse, CommonLocator.inputDataFieldName, "warehouse");
    }
    
    public void enterProjectionLinkage(String projectionlinkage){
        getUiActions().typeByAttributeName(projectionlinkage, CommonLocator.inputDataFieldName, "projection_linkage");
        getUiActions().waitForSeconds(3);
        getUiActions().pressEnterButton(By.xpath(String.format(CommonLocator.inputDataFieldName,"projection_linkage")));
    }
    
    public void enterProjectionAmount(String projectionamount){
        getUiActions().typeByAttributeNameAndHitTab(projectionamount, CommonLocator.inputDataFieldName, "projection_amount");
    }

    public void enterOrderPercentage(String orderpercentage){
        getUiActions().typeByAttributeNameAndHitTab(orderpercentage, CommonLocator.inputDataFieldName, "order_percentage");
    }

    public void enterSheduleDate(String sheduleddate){
        getUiActions().typeByAttributeNameAndHitTab(sheduleddate, CommonLocator.inputDataFieldName, "schedule_date");
    }

    

    public void setDate(){
        getUiActions().typeByAttributeNameAndHitEnter("t", CommonLocator.inputDataFieldName, "projection_start_date");
        getUiActions().typeByAttributeNameAndHitEnter("t", CommonLocator.inputDataFieldName, "projection_end_date");

    }

    

    public void getNameId() {
        getUiActions().waitForVisibilityOfElementLocated(nameId);
        procurementProjectionLogID = getUiActions().getText(nameId);
        log.info("procurementProjectionLogID:" + procurementProjectionLogID);
    }

    public void searchForId() {
        getUiActions().waitForVisibilityOfElementLocated(ID);
        getUiActions().driver.findElement(ID).sendKeys(procurementProjectionLogID, Keys.ENTER);

    }

    public void copyMRStatus() {
        getUiActions().waitForPresenceOfElementLocated(mrStatus);
        materialRequestID = getUiActions().getText(mrStatus);
        log.info("Copy of MR status: " + materialRequestID);
    }

    public void pasteMRStatus() {
        getUiActions().driver.findElement(pasteMRID).click();
        getUiActions().driver.findElement(pasteMRID).sendKeys(materialRequestID + Keys.ENTER);
        log.info("mr id entered");
    }

    public void clickMRID() throws InterruptedException {

        getUiActions().click(By.xpath("//a[@class='ellipsis']"));
        log.info("Material Request is created successfully");
    }

    public void createMR(List<String> list) {
        String projectionLinkage = (String) list.get(0);
        projectionLinkage = AutomationUtils.getTestData(projectionLinkage);
        log.info(projectionLinkage);
        String warehouse = (String) list.get(1);
        warehouse = AutomationUtils.getTestData(warehouse);
        log.info(warehouse);
        String projectionAmount = (String) list.get(2);
        projectionAmount = AutomationUtils.getTestData(projectionAmount);
        log.info(projectionAmount);
        String orderPercentage = (String) list.get(3);
        orderPercentage = AutomationUtils.getTestData(orderPercentage);
        log.info(orderPercentage);
    }
}

