let items = [1, 2, 3, 4, 5, 5];

let eachFn = require("../each");

function cb(item, index, arr) {
  return item+2;
}

let result = eachFn(items, cb);
console.log(result);

