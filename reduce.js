function reduce(elements, cb, startValue) {
  if (elements.length === 0) {
    return [];
  }
  let index = 0;
  if (startValue === undefined) {
    startValue = elements[0];
    index = 1;
  }
  for (index; index < elements.length; index++) {
    startValue = cb(startValue, elements[index], index, elements);
  }
  return startValue;
}
module.exports = reduce;